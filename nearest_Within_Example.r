# to do 
# add assertions so that the required inputs are used or helpful warning given
# make the column number specifiers work. 



library(tidyverse)
library(readxl)
library(sf)


schools_locations <- read_csv('directory.csv', 
                              skip = 0, 
                              name_repair = "universal") %>%
  select(School.Number, School.Name, Latitude, Longitude) %>%
  filter(!is.na(Longitude)) %>%
  st_as_sf(coords = c('Longitude', 'Latitude'), 
           crs = 4326)

ece_locations <- read_csv('directory_ece.csv', 
                          skip = 15, 
                          name_repair = "universal") %>%
  select(Service.Number, Service.Name, Latitude, Longitude) %>%
  filter(!is.na(Longitude)) %>%
  st_as_sf(coords = c('Longitude', 'Latitude'), 
           crs = 4326) %>%
  rename(ECE.Number = Service.Number, 
         ECE.Name = Service.Name)

nearest_within_x_meters <- function(location_df_a, 
                                    location_df_b,
                                    df_a_id_col_num = 1,
                                    df_b_id_col_num = 1,
                                    x_meters_limit = Inf) {
  
df_a_with_Nearest <- data.frame(df_a_id = NULL, 
                               nearest_df_b_id = NULL,
                               Distance_meters = NULL)

progression <- txtProgressBar(title = "Finding Nearest", 
                              initial = 0, 
                              max =  nrow(location_df_a[,1]), 
                              style = 3)

for (i in 1:nrow(location_df_a[,1])) {
  
  
  dfa_row_i <- location_df_a[i, ] 
  
  nearest_b_indx <- st_nearest_feature(dfa_row_i,
                                       location_df_b)
  
  nearest_b_id <- location_df_b[[nearest_b_indx, 1]]
  
  nearest_b <- location_df_b[nearest_b_indx, ]
  
  distance_to_nearest_b_m_i <- st_distance(dfa_row_i, 
                                           nearest_b) %>%
                                           as.numeric()
  
  new_row <- data.frame(a_id = dfa_row_i[[1, 1]], 
                        nearest_b_id = nearest_b_id,
                        Distance_2_Nearest_b_meters =  distance_to_nearest_b_m_i)
  
 df_a_with_Nearest <- rbind(df_a_with_Nearest, new_row)
 
  setTxtProgressBar(progression, i, title = "Finding Nearest")

}

total_locations <- nrow(location_df_a[,1])
  
locations_with_no_match_within_limit <- sum(df_a_with_Nearest$Distance_2_Nearest_b_meters > x_meters_limit) 

df_a_with_Nearest <- df_a_with_Nearest%>% 
  mutate(nearest_b_id = if_else(Distance_2_Nearest_b_meters > x_meters_limit, NA_real_, 
                                nearest_b_id),
         Distance_2_Nearest_b_meters = if_else(Distance_2_Nearest_b_meters > x_meters_limit, NA_real_, 
                                               Distance_2_Nearest_b_meters)) %>%
  mutate(Distance_2_Nearest_b_km = round(Distance_2_Nearest_b_meters/1000, 0))
         

message('\n Number Of Locations Processed ',total_locations, 
        '\n Number Of Locations Searched For Nearest ', nrow(location_df_b), 
        '\n Number Of Locations With No Match Within ', x_meters_limit,  ' (m) Limit ', 
        locations_with_no_match_within_limit)

df_a_with_Nearest

}


